package main

import (
	indexType "alexandre/crawler/pkg/types"
	"encoding/json"
	"fmt"
	"log"
	"net"
	"net/http"

	"github.com/davecgh/go-spew/spew"
	"github.com/gorilla/mux"
)

func main() {
	// Router de gorilla/mux pour la gestion des routes HTTP
	router := mux.NewRouter()

	// Endpoint pour la recherche
	router.HandleFunc("/createURL", createUrlHandler).Methods("POST")
	router.HandleFunc("/getFiles", getFilesHandler).Methods("POST")
	router.HandleFunc("/getSite", getSitesHandler).Methods("POST")

	// Adresse d'écoute du serveur HTTP
	listenAddress := "127.0.0.1"
	listenPort := "8000"

	// Démarrer le serveur HTTP
	fmt.Printf("Serveur HTTP démarré sur http://%s:%s\n", listenAddress, listenPort)
	log.Fatal(http.ListenAndServe(listenAddress+":"+listenPort, router))
}

func createUrlHandler(resWriter http.ResponseWriter, req *http.Request) {
	// Lire le corps (body) de la requête
	var requestData struct {
		URL string `json:"url"`
	}
	err := json.NewDecoder(req.Body).Decode(&requestData)
	if err != nil {
		http.Error(resWriter, "Erreur lors de la lecture du corps de la requête", http.StatusBadRequest)
		return
	}

	if requestData.URL == "" {
		http.Error(resWriter, "URL manquante", http.StatusBadRequest)
		log.Println("URL manquante")
		return
	}

	conn, err := net.Dial("tcp", "localhost:8080")
	if err != nil {
		log.Fatal(err)
	}
	defer conn.Close()

	request := indexType.CreateSiteRequest{
		GenericRequest: indexType.GenericRequest{
			Command: "createSite",
		},
		Url: requestData.URL,
	}

	requestBuffer, err := json.Marshal(request)
	if err != nil {
		log.Fatal(err)
	}

	conn.Write(requestBuffer)
	spew.Println("Sent :", request)

	received := make([]byte, 1024)
	_, err = conn.Read(received)
	if err != nil {
		fmt.Println("Read data failed:", err.Error())
	}

	fmt.Println(string(received))

	// Spécifier le type de contenu JSON dans l'en-tête de la réponse
	resWriter.Header().Set("Content-Type", "application/json")

	// Écrire la réponse JSON dans le corps de la réponse
	resWriter.Write([]byte("ok"))

}

func getFilesHandler(resWriter http.ResponseWriter, req *http.Request) {
	// Lire le corps (body) de la requête
	var requestData struct {
		Search string `json:"search"`
	}
	err := json.NewDecoder(req.Body).Decode(&requestData)
	if err != nil {
		http.Error(resWriter, "Erreur lors de la lecture du corps de la requête", http.StatusBadRequest)
		return
	}

	conn, err := net.Dial("tcp", "localhost:8080")
	if err != nil {
		log.Fatal(err)
	}
	defer conn.Close()

	request := indexType.GetFileRequest{
		GenericRequest: indexType.GenericRequest{
			Command: "getFile",
		},
		Params: requestData.Search,
	}

	requestBuffer, err := json.Marshal(request)
	if err != nil {
		log.Fatal(err)
	}

	conn.Write(requestBuffer)
	spew.Println("Sent :", request)

	received := make([]byte, 1024)
	n, err := conn.Read(received)
	if err != nil {
		fmt.Println("Read data failed:", err.Error())
	}

	fmt.Println(string(received))

	// Spécifier le type de contenu JSON dans l'en-tête de la réponse
	resWriter.Header().Set("Content-Type", "application/json")

	resWriter.Write(received[0:n])
}

func getSitesHandler(resWriter http.ResponseWriter, req *http.Request) {
	// Lire le corps (body) de la requête
	var requestData struct {
		Params string `json:"params"`
	}
	err := json.NewDecoder(req.Body).Decode(&requestData)
	if err != nil {
		http.Error(resWriter, "Erreur lors de la lecture du corps de la requête", http.StatusBadRequest)
		return
	}

	if requestData.Params == "" {
		http.Error(resWriter, "Recherche manquante: oldest ou new", http.StatusBadRequest)
		log.Println("Recherche manquante: oldest ou new")
		return
	}

	conn, err := net.Dial("tcp", "localhost:8080")
	if err != nil {
		log.Fatal(err)
	}
	defer conn.Close()

	request := indexType.GetSiteRequest{
		GenericRequest: indexType.GenericRequest{
			Command: "getSite",
		},
		Params: requestData.Params,
	}

	requestBuffer, err := json.Marshal(request)
	if err != nil {
		log.Fatal(err)
	}

	conn.Write(requestBuffer)
	spew.Println("Sent :", request)

	received := make([]byte, 1024)
	n, err := conn.Read(received)
	if err != nil {
		fmt.Println("Read data failed:", err.Error())
	}

	fmt.Println(string(received))

	// Spécifier le type de contenu JSON dans l'en-tête de la réponse
	resWriter.Header().Set("Content-Type", "application/json")

	resWriter.Write(received[0:n])
}
