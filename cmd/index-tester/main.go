package main

import (
	indexType "alexandre/crawler/pkg/types"
	"encoding/json"
	"fmt"
	"log"
	"net"
	"time"

	"github.com/davecgh/go-spew/spew"
)

func main() {
	spew.Println("Launch client")

	results := make(chan string)
	requestNumber := 0

	siteCreation := 3
	siteUpdate := 0
	siteGet := 0

	fileCreation := 0
	fileUpdate := 0
	fileGet := 0

	sitesToAdd := []string{"pkg.go.dev", "google.com", "bouteille93.de"}

	for i := 0; i < siteCreation; i++ {
		requestNumber++
		go createSite(sitesToAdd[i], results)
	}

	for i := 0; i < siteGet; i++ {
		requestNumber++
		spew.Println("Launch client get site")
		go getSite("oldest", results)
	}

	site := indexType.Site{
		Id:       2,
		HostIP:   "5.6.7.8",
		Domain:   "site2.com",
		Lastseen: time.Now(),
	}

	for i := 0; i < siteUpdate; i++ {
		requestNumber++
		go updateSite(site, results)
	}

	file := indexType.File{
		Id:       1,
		Name:     "file",
		Url:      "site1.com/file1",
		SiteId:   1,
		Lastseen: time.Now(),
	}

	for i := 0; i < fileCreation; i++ {
		requestNumber++
		file.Name = fmt.Sprintf("file%d", i)
		file.Url = fmt.Sprintf("site1.com/file%d", i)
		go createFile(file, results)
	}

	for i := 0; i < fileGet; i++ {
		requestNumber++
		go getFile("", results)
	}

	fileToUpdate := indexType.File{
		Id:       3,
		Name:     "crypt.pdf",
		Url:      "site2.com/test/cryptf.pdf",
		SiteId:   2,
		Lastseen: time.Now(),
	}

	for i := 0; i < fileUpdate; i++ {
		requestNumber++
		go updateFile(fileToUpdate, results)
	}

	spew.Println("Number of requests:", requestNumber)

	// boucle pour les résultats
	for i := 0; i < requestNumber; i++ {
		spew.Println("Waiting for result")
		result := <-results
		spew.Println("Number of results:", i+1)
		fmt.Println("Result:", result)
	}

	spew.Println("End client")
}

func createSite(url string, results chan<- string) {
	fmt.Println("Launch client create site")
	conn, err := net.Dial("tcp", "localhost:8080")
	if err != nil {
		log.Fatal(err)
	}
	defer conn.Close()

	if err != nil {
		log.Fatal(err)
	}

	request := indexType.CreateSiteRequest{
		GenericRequest: indexType.GenericRequest{
			Command: "createSite",
		},
		Url: url,
	}

	requestBuffer, err := json.Marshal(request)
	if err != nil {
		log.Fatal(err)
	}

	conn.Write(requestBuffer)
	spew.Println("Sent :", request)

	received := make([]byte, 1024)
	_, err = conn.Read(received)
	if err != nil {
		fmt.Println("Read data failed:", err.Error())
	}

	fmt.Println(string(received))
	conn.Close()
	results <- string(received)
}

func getSite(params string, results chan<- string) {
	fmt.Println("Launch client get site")
	conn, err := net.Dial("tcp", "localhost:8080")
	if err != nil {
		log.Fatal(err)
	}
	defer conn.Close()

	if err != nil {
		log.Fatal(err)
	}

	request := indexType.GetSiteRequest{
		GenericRequest: indexType.GenericRequest{
			Command: "getSite",
		},
		Params: params,
	}

	requestBuffer, err := json.Marshal(request)
	if err != nil {
		log.Fatal(err)
	}

	conn.Write(requestBuffer)
	spew.Println("Sent :", request)

	received := make([]byte, 1024)
	_, err = conn.Read(received)
	if err != nil {
		fmt.Println("Read data failed:", err.Error())
	}

	fmt.Println(string(received))
	conn.Close()
	results <- string(received)
}

func updateSite(site indexType.Site, results chan<- string) {
	fmt.Println("Launch client update site")
	conn, err := net.Dial("tcp", "localhost:8080")
	if err != nil {
		log.Fatal(err)
	}
	defer conn.Close()

	request := indexType.UpdateSiteRequest{
		GenericRequest: indexType.GenericRequest{
			Command: "updateSite",
		},
		Site: indexType.Site{
			Id:       site.Id,
			HostIP:   site.HostIP,
			Domain:   site.Domain,
			Lastseen: site.Lastseen,
		},
	}

	requestBuffer, err := json.Marshal(request)
	if err != nil {
		log.Fatal(err)
	}

	conn.Write(requestBuffer)
	spew.Println("Sent :", request)

	received := make([]byte, 1024)
	_, err = conn.Read(received)
	if err != nil {
		fmt.Println("Read data failed:", err.Error())
	}

	fmt.Println(string(received))
	conn.Close()

	results <- string(received)
}

func createFile(file indexType.File, results chan<- string) {
	fmt.Println("Launch client create file")
	conn, err := net.Dial("tcp", "localhost:8080")
	if err != nil {
		log.Fatal(err)
	}
	defer conn.Close()

	if err != nil {
		log.Fatal(err)
	}

	request := indexType.CreateFileRequest{
		GenericRequest: indexType.GenericRequest{
			Command: "createFile",
		},
		File: indexType.File{
			Id:       file.Id,
			SiteId:   file.SiteId,
			Url:      file.Url,
			Name:     file.Name,
			Lastseen: file.Lastseen,
		},
	}

	requestBuffer, err := json.Marshal(request)
	if err != nil {
		log.Fatal(err)
	}

	conn.Write(requestBuffer)
	spew.Println("Sent :", request)

	received := make([]byte, 1024)
	_, err = conn.Read(received)
	if err != nil {
		fmt.Println("Read data failed:", err.Error())
	}

	fmt.Println(string(received))
	conn.Close()
	results <- string(received)
}

func getFile(params string, results chan<- string) {
	fmt.Println("Launch client get file")
	conn, err := net.Dial("tcp", "localhost:8080")
	if err != nil {
		log.Fatal(err)
	}
	defer conn.Close()

	if err != nil {
		log.Fatal(err)
	}

	request := indexType.GetFileRequest{
		GenericRequest: indexType.GenericRequest{
			Command: "getFile",
		},
		Params: params,
	}

	requestBuffer, err := json.Marshal(request)
	if err != nil {
		log.Fatal(err)
	}

	conn.Write(requestBuffer)
	spew.Println("Sent :", request)

	received := make([]byte, 1024)
	_, err = conn.Read(received)
	if err != nil {
		fmt.Println("Read data failed:", err.Error())
	}

	fmt.Println(string(received))
	conn.Close()
	results <- string(received)
}

func updateFile(file indexType.File, results chan<- string) {
	fmt.Println("Launch client update file")
	conn, err := net.Dial("tcp", "localhost:8080")
	if err != nil {
		log.Fatal(err)
	}
	defer conn.Close()

	request := indexType.UpdateFileRequest{
		GenericRequest: indexType.GenericRequest{
			Command: "updateFile",
		},
		File: indexType.File{
			Id:       file.Id,
			SiteId:   file.SiteId,
			Url:      file.Url,
			Name:     file.Name,
			Lastseen: file.Lastseen,
		},
	}

	requestBuffer, err := json.Marshal(request)
	if err != nil {
		log.Fatal(err)
	}

	conn.Write(requestBuffer)
	spew.Println("Sent :", request)

	received := make([]byte, 1024)
	_, err = conn.Read(received)
	if err != nil {
		fmt.Println("Read data failed:", err.Error())
	}

	fmt.Println(string(received))
	conn.Close()

	results <- string(received)
}
