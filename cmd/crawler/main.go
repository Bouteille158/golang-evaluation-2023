package main

import (
	indexType "alexandre/crawler/pkg/types"
	"encoding/json"
	"fmt"
	"log"
	"net"
	"net/http"
	"net/url"
	"strings"
	"time"

	"github.com/PuerkitoBio/goquery"
	"github.com/davecgh/go-spew/spew"
)

func main() {
	spew.Println("Launch crawler")

	for {

		results := make(chan string)
		requestNumber := 0
		isSiteNew := true

		// Get new site
		fmt.Println("Getting new site")
		resultSite := make(chan indexType.Site)
		go getSite("new", results, resultSite)
		requestNumber++

		getSiteStatus := <-results
		siteToCrawl := <-resultSite

		fmt.Println("Status of new site request: ", getSiteStatus)
		fmt.Println("Result of new site request: ", siteToCrawl)

		// If no new site, get oldest site
		if getSiteStatus != "ok" {
			fmt.Println("No new site to crawl, getting oldest site")
			go getSite("oldest", results, resultSite)
			isSiteNew = false
			requestNumber++

			getSiteStatus := <-results
			siteToCrawl = <-resultSite
			fmt.Println("Status of oldest site request: ", getSiteStatus)
			fmt.Println("Result of oldest site request: ", siteToCrawl)

			// If no oldest site, wait 5 seconds and retry
			if getSiteStatus != "ok" {
				fmt.Println("No site to crawl, waiting 5 seconds")
				time.Sleep(5 * time.Second)
				continue
			}
		}

		fmt.Println("Result site: ", siteToCrawl)
		// Get ip for domain
		spew.Println("Getting ip for domain: ", siteToCrawl.Domain)
		ips, err := net.LookupIP(siteToCrawl.Domain)
		if err != nil {
			log.Println(err)
		}

		var ipv4 net.IP

		for _, ip := range ips {
			spew.Println(ip)
			if ipv4 = ip.To4(); ipv4 != nil {
				break
			}
			ipv4 = net.IPv4(0, 0, 0, 0)
		}

		// Update site info	(HostIP, Lastseen)
		site := indexType.Site{
			Id:       siteToCrawl.Id,
			HostIP:   ipv4.String(),
			Domain:   siteToCrawl.Domain,
			Lastseen: time.Now(),
		}

		spew.Println("Site to update: ", site)

		if siteToCrawl.Id != 0 {
			fmt.Println("Updating site")
			go updateSite(site, results)

			updateSiteStatus := <-results
			fmt.Println("Status of update site request: ", updateSiteStatus)
		}

		// List files of site
		fmt.Println("Listing files of site")
		files, err := listFiles(siteToCrawl.Domain)
		if err != nil {
			log.Fatal(err)
		}

		if isSiteNew {
			//Create files in index
			fmt.Println("Creating files in index")
			creationResults := make(chan string)
			creatRequestNumber := 0

			for _, file := range files {
				fileToCreate := indexType.File{
					Name:     file.Name,
					Url:      file.Url,
					SiteId:   siteToCrawl.Id,
					Lastseen: time.Now(),
				}
				go createFile(fileToCreate, creationResults)
				creatRequestNumber++
			}

			for i := 0; i < creatRequestNumber; i++ {
				createFileStatus := <-creationResults
				fmt.Println("Status number", i+1, "out of", creatRequestNumber, "of create file request:", createFileStatus)

			}
		} else {
			//Update files in index
			fmt.Println("Updating files in index")
			updateResults := make(chan string)
			getFileStatusChan := make(chan string)
			updateRequestNumber := 0

			for _, file := range files {
				fileSearch := make(chan []indexType.File)
				go getFile(file.Name, getFileStatusChan, fileSearch)
				getFileStatus := <-getFileStatusChan
				fmt.Println("Status of get file request: ", getFileStatus)

				filesFound := <-fileSearch
				spew.Println("Files found: ", filesFound)

				if len(filesFound) == 0 {

					fileToCreate := indexType.File{
						Name:     file.Name,
						Url:      file.Url,
						SiteId:   siteToCrawl.Id,
						Lastseen: time.Now(),
					}
					go createFile(fileToCreate, updateResults)

					for _, fileFound := range filesFound {
						if fileFound.Url == file.Url && fileFound.Name == file.Name {

							fileToUpdate := indexType.File{
								Id:       fileFound.Id,
								Name:     file.Name,
								Url:      file.Url,
								SiteId:   siteToCrawl.Id,
								Lastseen: time.Now(),
							}
							go updateFile(fileToUpdate, updateResults)
							updateRequestNumber++
						}
					}
				}
				for i := 0; i < updateRequestNumber; i++ {
					updateFileStatus := <-updateResults
					fmt.Println("Status number", i+1, "out of", updateRequestNumber, "of update file request:", updateFileStatus)
				}
			}

			time.Sleep(2 * time.Second)
		}
	}
}

func createSite(url string, results chan<- string) {
	fmt.Println("Launch client create site")
	conn, err := net.Dial("tcp", "localhost:8080")
	if err != nil {
		log.Fatal(err)
	}
	defer conn.Close()

	if err != nil {
		log.Fatal(err)
	}

	request := indexType.CreateSiteRequest{
		GenericRequest: indexType.GenericRequest{
			Command: "createSite",
		},
		Url: url,
	}

	requestBuffer, err := json.Marshal(request)
	if err != nil {
		log.Fatal(err)
	}

	conn.Write(requestBuffer)
	spew.Println("Sent :", request)

	received := make([]byte, 1024)
	_, err = conn.Read(received)
	if err != nil {
		fmt.Println("Read data failed:", err.Error())
	}

	fmt.Println(string(received))
	conn.Close()
	results <- string(received)
}

func getSite(params string, results chan<- string, resultSite chan<- indexType.Site) {
	fmt.Println("Launch client get site")
	conn, err := net.Dial("tcp", "localhost:8080")
	if err != nil {
		log.Fatal(err)
	}
	defer conn.Close()

	request := indexType.GetSiteRequest{
		GenericRequest: indexType.GenericRequest{
			Command: "getSite",
		},
		Params: params,
	}

	requestBuffer, err := json.Marshal(request)
	if err != nil {
		log.Fatal(err)
	}

	conn.Write(requestBuffer)
	spew.Println("Sent :", request)

	receivedBuffer := make([]byte, 1024)
	n, err := conn.Read(receivedBuffer)
	if err != nil {
		fmt.Println("Read data failed:", err.Error())
		results <- "error"
	}

	fmt.Println(string(receivedBuffer[0:n]))
	conn.Close()

	fmt.Println("Received buffer :", receivedBuffer[0:n])

	var received indexType.GetSiteResponse
	err = json.Unmarshal(receivedBuffer[0:n], &received)
	if err != nil {
		fmt.Println("Unmarshal failed:", err.Error())
		results <- "error"
	}

	results <- received.GenericResponse.Status

	if received.GenericResponse.Status == "ok" {
		resultSite <- received.Site
	} else {
		resultSite <- indexType.Site{}
	}

}

func updateSite(site indexType.Site, results chan<- string) {
	fmt.Println("Launch client update site")
	conn, err := net.Dial("tcp", "localhost:8080")
	if err != nil {
		log.Fatal(err)
	}
	defer conn.Close()

	request := indexType.UpdateSiteRequest{
		GenericRequest: indexType.GenericRequest{
			Command: "updateSite",
		},
		Site: indexType.Site{
			Id:       site.Id,
			HostIP:   site.HostIP,
			Domain:   site.Domain,
			Lastseen: site.Lastseen,
		},
	}

	requestBuffer, err := json.Marshal(request)
	if err != nil {
		log.Fatal(err)
		results <- "error"
	}

	conn.Write(requestBuffer)
	spew.Println("Sent :", request)

	receivedBuffer := make([]byte, 1024)
	n, err := conn.Read(receivedBuffer)
	if err != nil {
		fmt.Println("Read data failed:", err.Error())
	}

	fmt.Println(string(receivedBuffer[0:n]))
	conn.Close()

	// Decode response
	var received indexType.GenericResponse
	err = json.Unmarshal(receivedBuffer[0:n], &received)
	if err != nil {
		log.Println(err)
		results <- "error"
	}

	results <- received.Status

}

func createFile(file indexType.File, results chan<- string) {
	fmt.Println("Launch client create file")
	conn, err := net.Dial("tcp", "localhost:8080")
	if err != nil {
		log.Fatal(err)
	}
	defer conn.Close()

	if err != nil {
		log.Fatal(err)
	}

	request := indexType.CreateFileRequest{
		GenericRequest: indexType.GenericRequest{
			Command: "createFile",
		},
		File: indexType.File{
			Id:       file.Id,
			SiteId:   file.SiteId,
			Url:      file.Url,
			Name:     file.Name,
			Lastseen: file.Lastseen,
		},
	}

	requestBuffer, err := json.Marshal(request)
	if err != nil {
		log.Fatal(err)
	}

	conn.Write(requestBuffer)
	spew.Println("Sent :", request)

	received := make([]byte, 1024)
	_, err = conn.Read(received)
	if err != nil {
		fmt.Println("Read data failed:", err.Error())
	}

	fmt.Println(string(received))
	conn.Close()
	results <- string(received)
}

func getFile(params string, results chan<- string, resultFiles chan<- []indexType.File) {
	fmt.Println("Launch client get file")
	conn, err := net.Dial("tcp", "localhost:8080")
	if err != nil {
		log.Fatal(err)
	}
	defer conn.Close()

	request := indexType.GetFileRequest{
		GenericRequest: indexType.GenericRequest{
			Command: "getFile",
		},
		Params: params,
	}

	requestBuffer, err := json.Marshal(request)
	if err != nil {
		log.Fatal(err)
	}

	conn.Write(requestBuffer)
	spew.Println("Sent :", request)

	receivedBuffer := make([]byte, 1024)
	n, err := conn.Read(receivedBuffer)
	if err != nil {
		fmt.Println("Read data failed:", err.Error())
	}

	fmt.Println("Received buffer:", string(receivedBuffer))
	conn.Close()

	//Decode response
	var received indexType.GetFileResponse
	err = json.Unmarshal(receivedBuffer[0:n], &received)
	if err != nil {
		fmt.Println("Unmarshal failed:", err.Error())
		results <- "error"
	}

	results <- received.GenericResponse.Status

	if received.GenericResponse.Status == "ok" {
		resultFiles <- received.Files
	} else {
		resultFiles <- []indexType.File{}
	}
	fmt.Println("End client get file")
}

func updateFile(file indexType.File, results chan<- string) {
	fmt.Println("Launch client update file")
	conn, err := net.Dial("tcp", "localhost:8080")
	if err != nil {
		log.Fatal(err)
	}
	defer conn.Close()

	request := indexType.UpdateFileRequest{
		GenericRequest: indexType.GenericRequest{
			Command: "updateFile",
		},
		File: indexType.File{
			Id:       file.Id,
			SiteId:   file.SiteId,
			Url:      file.Url,
			Name:     file.Name,
			Lastseen: file.Lastseen,
		},
	}

	requestBuffer, err := json.Marshal(request)
	if err != nil {
		log.Fatal(err)
	}

	conn.Write(requestBuffer)
	spew.Println("Sent :", request)

	received := make([]byte, 1024)
	_, err = conn.Read(received)
	if err != nil {
		fmt.Println("Read data failed:", err.Error())
	}

	fmt.Println(string(received))
	conn.Close()

	results <- string(received)
}

func listFiles(domain string) ([]simpleFile, error) {
	files := []simpleFile{}

	// Récupération du contenu HTML de la page d'accueil
	resp, err := http.Get("http://" + domain)
	if err != nil {
		return files, err
	}
	defer resp.Body.Close()

	// Analyse du HTML avec goquery
	doc, err := goquery.NewDocumentFromReader(resp.Body)
	if err != nil {
		return files, err
	}

	// Recherche de tous les liens dans la page
	doc.Find("a").Each(func(i int, s *goquery.Selection) {
		href, exists := s.Attr("href")
		spew.Println("Href: ", href)
		if exists {
			// Vérification si le lien pointe vers un fichier
			if isFile(href) {
				spew.Println("File found: ", href)
				newFile := simpleFile{
					Name: href,
					Url:  domain + "/" + href,
				}
				files = append(files, newFile)
			}
			// Vérification si le lien pointe vers un dossier
			if isDirectory(href, domain) {
				spew.Println("Directory found: ", href)
				// coller le domaine au lien
				domainhref := domain + "/" + href
				spew.Println("Domain href to search: ", domainhref)
				additionnalFiles, err := listFiles(domainhref)
				if err != nil {
					log.Fatal(err)
				}
				files = append(files, additionnalFiles...)
			}
		}
	})

	return files, nil
}

func isFile(href string) bool {
	// Vérification si le lien pointe vers un fichier
	u, err := url.Parse(href)
	if err != nil {
		return false
	}
	path := u.Path
	if strings.HasSuffix(path, ".html") || strings.HasSuffix(path, ".htm") || strings.HasPrefix(href, "http") || strings.HasPrefix(href, "www") {
		return false
	}
	if strings.HasSuffix(path, "/") {
		return false
	}
	// Vérification si le lien pointe vers un fichier
	if strings.Contains(path, ".") {
		return true
	}
	return false
}

func isDirectory(href string, domain string) bool {
	// Vérification si le lien pointe vers un dossier
	u, err := url.Parse(href)
	if err != nil {
		return false
	}
	path := u.Path
	if strings.HasPrefix(href, "http") || strings.HasPrefix(href, "www") || strings.Contains(href, "..") {
		return false
	}
	if strings.Contains(domain, path) {
		return false
	}
	if strings.HasSuffix(path, "/") && path != "/" {
		if !strings.HasSuffix(path, "../") || !strings.HasSuffix(path, "./") || !strings.HasSuffix(path, "/") {
			return true
		}
	}
	return false
}

type simpleFile struct {
	Name string `json:"name"`
	Url  string `json:"url"`
}
