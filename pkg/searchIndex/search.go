package searchIndex

// Structure pour représenter un fichier
type File struct {
	URL   string `json:"url"`
	Title string `json:"title"`
}

// Structure de la réponse JSON
type SearchResult struct {
	Results []string `json:"results"`
}
