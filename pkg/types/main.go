package types

import (
	"time"
)

type Site struct {
	Id       int64
	HostIP   string
	Domain   string
	Lastseen time.Time
}

type File struct {
	Id       int64
	Name     string
	Url      string
	SiteId   int64
	Lastseen time.Time
}

type GenericRequest struct {
	Command string `json:"command"`
}
type GenericResponse struct {
	Status string `json:"status"`
}
type CreateSiteRequest struct {
	GenericRequest
	Url string `json:"url"`
}

type CreateSiteResponse struct {
	GenericResponse
	Id int64 `json:"id"`
}

type UpdateSiteRequest struct {
	GenericRequest
	Site Site `json:"site"`
}

type UpdateSiteResponse struct {
	GenericResponse
}

type GetSiteRequest struct {
	GenericRequest
	Params string `json:"params"`
}

type GetSiteResponse struct {
	GenericResponse
	Site Site `json:"site"`
}

type CreateFileRequest struct {
	GenericRequest
	File File `json:"file"`
}

type CreateFileResponse struct {
	GenericResponse
	Id int64 `json:"id"`
}

type GetFileRequest struct {
	GenericRequest
	Params string `json:"params"`
}

type GetFileResponse struct {
	GenericResponse
	Files []File `json:"files"`
}

type UpdateFileRequest struct {
	GenericRequest
	File File `json:"file"`
}

type UpdateFileResponse struct {
	GenericResponse
}
