package main

import (
	indexType "alexandre/crawler/pkg/types"
	"encoding/json"
	"fmt"
	"log"
	"net"
	"strings"
	"time"

	"github.com/davecgh/go-spew/spew"
)

var sites []indexType.Site
var files []indexType.File

func main() {
	ln, err := net.Listen("tcp", "0.0.0.0:8080")
	fmt.Println("Launch server on port 8080")
	if err != nil {
		log.Fatal(err)
	}

	defer ln.Close()

	for {
		conn, err := ln.Accept()
		if err != nil {
			log.Println(err)
			continue
		}

		go handleConnection(conn)

	}
}

func handleConnection(conn net.Conn) {

	defer conn.Close()

	spew.Println("New connection =========================================================================================================================")
	spew.Println("Sites : ", sites)

	buffer := make([]byte, 1024)
	n, err := conn.Read(buffer)
	if err != nil {
		log.Fatal(err)
	}

	var request indexType.GenericRequest

	json.Unmarshal(buffer[0:n], &request)

	spew.Println("Requête :", request.Command)

	responseBuffer := make([]byte, 4096)
	spew.Println("Réponse buffer :", responseBuffer[0:0])

	responseBufferChannel := make(chan []byte, 4096)

	switch request.Command {
	case "createSite":
		go handleCreateSite(conn, buffer, n, responseBufferChannel)
	case "getSite":
		go handleGetSite(conn, buffer, n, responseBufferChannel)
	case "updateSite":
		go handleUpdateSite(conn, buffer, n, responseBufferChannel)
	case "createFile":
		go handleCreateFile(conn, buffer, n, responseBufferChannel)
	case "getFile":
		go handleGetFile(conn, buffer, n, responseBufferChannel)
	case "updateFile":
		go handleUpdateFile(conn, buffer, n, responseBufferChannel)

	default:
		fmt.Println("Command not found")
	}

	time := time.Now().Format(time.ANSIC)
	responseStr := fmt.Sprintf("Your message is: %s. Received time: %s", string(buffer[0:n]), time)
	spew.Println(responseStr)

	responseBuffer = <-responseBufferChannel

	conn.Write(responseBuffer)
	spew.Println("Response sent to client")
}

func handleCreateSite(conn net.Conn, request []byte, n int, responseBufferChannel chan<- []byte) {
	spew.Println("Create site")
	spew.Println("request in bytes :")
	spew.Println(request[0:n])

	// Décoder la requête
	var createUrlRequest indexType.CreateSiteRequest
	err := json.Unmarshal(request[0:n], &createUrlRequest)
	if err != nil {
		log.Println(err)
	}

	spew.Println("createUrlRequest :")
	spew.Println(createUrlRequest)

	siteId := int64(len(sites) + 1)

	sites = append(sites, indexType.Site{
		Domain:   createUrlRequest.Url,
		Id:       siteId,
		HostIP:   "",
		Lastseen: time.Time{},
	})

	createUrlResponse := indexType.CreateSiteResponse{
		Id: siteId,
		GenericResponse: indexType.GenericResponse{
			Status: "ok",
		},
	}

	spew.Println("createUrlResponse :")
	spew.Println(createUrlResponse)

	responseBuffer, err := json.Marshal(createUrlResponse)
	if err != nil {
		log.Println(err)
	}

	spew.Println("responseBuffer :")
	spew.Println(responseBuffer)

	responseBufferChannel <- responseBuffer
	spew.Println("responseBuffer sent to channel")

	spew.Println("sites :")
	spew.Println(sites)

}

func handleGetSite(conn net.Conn, request []byte, n int, responseBufferChannel chan<- []byte) {
	spew.Println("Get site")
	spew.Println("request in bytes :")
	spew.Println(request[0:n])

	// Décoder la requête
	var getUrlRequest indexType.GetSiteRequest
	err := json.Unmarshal(request[0:n], &getUrlRequest)
	if err != nil {
		log.Println(err)
	}

	spew.Println("getUrlRequest :")
	spew.Println(getUrlRequest)

	site := indexType.Site{
		Id:       0,
		HostIP:   "",
		Lastseen: time.Time{},
		Domain:   "",
	}

	switch getUrlRequest.Params {
	case "oldest":
		spew.Println("oldest")
		site = findOldestSite(sites)
	case "new":
		spew.Println("new")
		for _, s := range sites {
			spew.Println("site in loop :")
			spew.Println(s)

			if s.Lastseen.IsZero() {
				fmt.Println("Found new site")
				site = s
				break
			}
		}
		fmt.Println("No new site found")
	}

	spew.Println("site: ")
	spew.Println(site)

	if site.Id == 0 {
		getUrlResponse := indexType.GenericResponse{
			Status: "error: no site found",
		}
		responseBuffer, err := json.Marshal(getUrlResponse)
		if err != nil {
			log.Println(err)
		}

		spew.Println("responseBuffer :")
		spew.Println(responseBuffer)

		responseBufferChannel <- responseBuffer
		spew.Println("responseBuffer sent to channel")
		return
	}

	getUrlResponse := indexType.GetSiteResponse{
		GenericResponse: indexType.GenericResponse{
			Status: "ok",
		},
		Site: site,
	}

	spew.Println("getUrlResponse :")
	spew.Println(getUrlResponse)

	responseBuffer, err := json.Marshal(getUrlResponse)
	if err != nil {
		log.Println(err)
	}

	spew.Println("responseBuffer :")
	spew.Println(responseBuffer)

	responseBufferChannel <- responseBuffer
	spew.Println("responseBuffer sent to channel")
}

func handleUpdateSite(conn net.Conn, request []byte, n int, responseBufferChannel chan<- []byte) {
	spew.Println("Update site")
	spew.Println("request in bytes :")
	spew.Println(request[0:n])

	// Décoder la requête
	var updateUrlRequest indexType.UpdateSiteRequest
	err := json.Unmarshal(request[0:n], &updateUrlRequest)
	if err != nil {
		log.Println(err)
		updateUrlResponse := indexType.GenericResponse{
			Status: "error: can't parse update request",
		}
		responseBuffer, err := json.Marshal(updateUrlResponse)
		if err != nil {
			log.Println(err)
		}

		spew.Println("responseBuffer :")
		spew.Println(responseBuffer)

		responseBufferChannel <- responseBuffer
		spew.Println("responseBuffer sent to channel")
		return
	}

	siteAdded := false
	for n, site := range sites {
		spew.Println("Searching existing site")

		fmt.Println("site in loop :")
		fmt.Println(site)

		if site.Id == updateUrlRequest.Site.Id {
			spew.Println("site found")
			sites[n] = updateUrlRequest.Site
			spew.Printf("sites[%d] updated with site %v", n, updateUrlRequest.Site)
			siteAdded = true
			break
		}
	}
	fmt.Println("Is the site found ?")
	fmt.Println(siteAdded)
	if !siteAdded {
		sites = append(sites, updateUrlRequest.Site)
	}

	spew.Println("updateUrlRequest :")
	spew.Println(updateUrlRequest)

	updateUrlResponse := indexType.UpdateSiteResponse{
		GenericResponse: indexType.GenericResponse{
			Status: "ok",
		},
	}

	spew.Println("updateUrlResponse :")
	spew.Println(updateUrlResponse)

	responseBuffer, err := json.Marshal(updateUrlResponse)
	if err != nil {
		log.Println(err)
	}

	spew.Println("responseBuffer :")
	spew.Println(responseBuffer)

	responseBufferChannel <- responseBuffer
	spew.Println("responseBuffer sent to channel")

	spew.Println("sites :")
	spew.Println(sites)
}

func handleCreateFile(conn net.Conn, request []byte, n int, responseBufferChannel chan<- []byte) {
	spew.Println("Create file")
	spew.Println("request in bytes :")
	spew.Println(request[0:n])

	// Décoder la requête
	var createFileRequest indexType.CreateFileRequest
	err := json.Unmarshal(request[0:n], &createFileRequest)
	if err != nil {
		log.Println(err)
	}

	spew.Println("createFileRequest :")
	spew.Println(createFileRequest)

	fileId := int64(len(files) + 1)

	files = append(files, indexType.File{
		Name:     createFileRequest.File.Name,
		Id:       fileId,
		Lastseen: createFileRequest.File.Lastseen,
		Url:      createFileRequest.File.Url,
		SiteId:   createFileRequest.File.SiteId,
	})

	createFileResponse := indexType.CreateFileResponse{
		Id: fileId,
		GenericResponse: indexType.GenericResponse{
			Status: "ok",
		},
	}

	spew.Println("createFileResponse :")
	spew.Println(createFileResponse)

	responseBuffer, err := json.Marshal(createFileResponse)
	if err != nil {
		log.Println(err)
	}

	spew.Println("responseBuffer :")
	spew.Println(responseBuffer)

	responseBufferChannel <- responseBuffer
	spew.Println("responseBuffer sent to channel")

	spew.Println("files :")
	spew.Println(files)
}

func handleGetFile(conn net.Conn, request []byte, n int, responseBufferChannel chan<- []byte) {
	spew.Println("Get file")
	spew.Println("request in bytes :")

	// Décoder la requête
	var getFileRequest indexType.GetFileRequest
	err := json.Unmarshal(request[0:n], &getFileRequest)
	if err != nil {
		log.Println(err)
	}

	spew.Println("getFileRequest :")
	spew.Println(getFileRequest)

	keywords := strings.Split(getFileRequest.Params, " ")

	filesToSend := []indexType.File{}

	for _, file := range files {
		spew.Println("Checking file :")
		spew.Println(file)
		for _, keyword := range keywords {
			spew.Println("Checking keyword :")
			spew.Println(keyword)
			if strings.Contains(file.Name, keyword) {
				spew.Printf("keyword %s found in file %s\n", keyword, file.Name)
				filesToSend = append(filesToSend, file)
				break
			}
		}
	}

	spew.Println("filesToSend :")
	spew.Println(filesToSend)

	getFileResponse := indexType.GetFileResponse{
		GenericResponse: indexType.GenericResponse{
			Status: "ok",
		},
		Files: filesToSend,
	}

	spew.Println("getFileResponse :")
	spew.Println(getFileResponse)

	responseBuffer, err := json.Marshal(getFileResponse)
	if err != nil {
		log.Println(err)
	}

	spew.Println("responseBuffer :")
	spew.Println(responseBuffer)

	responseBufferChannel <- responseBuffer
	spew.Println("responseBuffer sent to channel")

}

func handleUpdateFile(conn net.Conn, request []byte, n int, responseBufferChannel chan<- []byte) {
	spew.Println("Update file")
	spew.Println("request in bytes :")

	// Décoder la requête
	var updateFileRequest indexType.UpdateFileRequest
	err := json.Unmarshal(request[0:n], &updateFileRequest)
	if err != nil {
		log.Println(err)
	}

	spew.Println("updateFileRequest :")
	spew.Println(updateFileRequest)

	for n, file := range files {
		spew.Println("Searching :")
		spew.Println("Checking file :")
		spew.Println(file)
		fileAdded := false
		if file.Id == updateFileRequest.File.Id {
			spew.Println("file found with id :")
			spew.Println(file.Id)
			spew.Println("file :")
			spew.Println(file)
			files[n] = updateFileRequest.File
			fileAdded = true
			break
		}
		if !fileAdded {
			files = append(files, updateFileRequest.File)
		}
	}

	spew.Println("updateFileRequest :")
	spew.Println(updateFileRequest)

	updateFileResponse := indexType.UpdateFileResponse{
		GenericResponse: indexType.GenericResponse{
			Status: "ok",
		},
	}

	spew.Println("updateFileResponse :")
	spew.Println(updateFileResponse)

	responseBuffer, err := json.Marshal(updateFileResponse)
	if err != nil {
		log.Println(err)
	}

	spew.Println("responseBuffer :")
	spew.Println(responseBuffer)

	responseBufferChannel <- responseBuffer
	spew.Println("responseBuffer sent to channel")
}

func findOldestSite(sites []indexType.Site) indexType.Site {
	if len(sites) == 0 {
		return indexType.Site{}
	}
	oldestSite := sites[0]
	for _, site := range sites {
		if !site.Lastseen.IsZero() {
			if site.Lastseen.Before(oldestSite.Lastseen) {
				oldestSite = site
			}
		}
	}
	return oldestSite
}
